# Base image: https://hub.docker.com/_/golang/
FROM golang:1.14.2-buster

# Install golint
ENV GOPATH /go
ENV PATH ${GOPATH}/bin:$PATH
RUN go get -u golang.org/x/lint/golint

RUN apt-get update
RUN apt-get install -y apt-transport-https ca-certificates

# Add apt key for LLVM repository
RUN wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -

# Add LLVM apt repository
RUN echo "deb http://apt.llvm.org/buster/ llvm-toolchain-buster-9 main" | tee -a /etc/apt/sources.list

# Install clang from LLVM repository

RUN apt-get update && apt-get install -y --no-install-recommends \
    clang-9 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set Clang as default CC
ENV set_clang /etc/profile.d/set-clang-cc.sh
RUN echo "export CC=clang-5.0" | tee -a ${set_clang} && chmod a+x ${set_clang}

